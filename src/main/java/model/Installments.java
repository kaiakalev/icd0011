package model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Min;
import java.time.LocalDate;

@Data
public class Installments {
    @Min(3)
    private int amount;

    @JsonFormat(pattern = "yyy-MM-dd")
    private LocalDate date;

    public Installments(@Min(3) int amount, LocalDate date) {
        this.amount = amount;
        this.date = date;
    }
}
