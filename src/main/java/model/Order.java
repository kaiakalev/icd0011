package model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
//@NoArgsConstructor
@RequiredArgsConstructor
@Table(name="orders")
public class Order extends BaseEntity {

    @Size(min = 2)
    @Column(name="order_number")
    private String orderNumber;

    @Valid
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="order_rows",
            joinColumns=@JoinColumn(name = "orders_id",
                    referencedColumnName = "id"))
    @OrderBy("item_name")
    private Set<OrderRow> orderRows;


    @Override
    public String toString() {
        return "model.Order{" +
                "id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", orderRows=" + orderRows +
                '}';
    }
}
