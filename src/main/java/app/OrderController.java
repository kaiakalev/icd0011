package app;

import config.DbConfig;
import config.HsqlDataSource;
import dao.OrderDao;
import model.Installments;
import model.Order;
import model.OrderRow;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class OrderController {

   /* @Autowired
    private OrderMemoryDao dao;*/

   //loeme andmebaasi sisse
    private ConfigurableApplicationContext ctx =
            new AnnotationConfigApplicationContext(
                    DbConfig.class, HsqlDataSource.class);
    //dao
    private OrderDao dao = ctx.getBean(OrderDao.class);

  /*  @PostMapping("login")
    public User login(@RequestParam("user") String username, @RequestParam("password") String pwd) {

        String token = getJWTToken(username);
        User user = new User();
        user.setUserName(username);
        user.setToken(token);
        return user;

    }*/


    @GetMapping("orders")
    public List<Order> getPosts() {
        return dao.findAll();
    }

    @GetMapping("orders/{id}")
    public Order getOne(@PathVariable Long id) {
        return dao.findById(id);
    }

    @GetMapping("orders/{id}/installments")
    public List<Installments> getInstallments(@PathVariable Long id,
                                  @RequestParam("start")
                                  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
                                  @RequestParam("end")
                                  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end) {
        Order order = dao.findById(id);
        int price = 0;
        for(OrderRow ow : order.getOrderRows()){
           price += ow.getPrice();
        }

        long yearsInBetween = end.getYear()-start.getYear();
        long monthsDiff = end.getMonthValue()-start.getMonthValue();

        if(start.getDayOfMonth() > end.getDayOfMonth()){
            monthsDiff++;
        }

        long ageInMonths = yearsInBetween*12 + monthsDiff;

        return calculateInstallments(price,ageInMonths,start,end);
    }

    @GetMapping("version")
    public String showVersion() {

        return "9 kodutöö";
    }

    @GetMapping("users/{userName}")
    @PreAuthorize(value = "hasRole('ADMIN')" + " or #userName == authentication.name")
    public String getUserByName(@PathVariable String userName, Authentication a, Principal p) {

        return userName;
    }


    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {
        return dao.save(order);
    }

   /* @DeleteMapping("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        dao.delete(id);
    }*/

    private List<Installments> calculateInstallments(int price, long ageInMonths, LocalDate start, LocalDate end){
        List<Long> osamaksed = new ArrayList<>();
        long jaak = (long) price % ageInMonths;
        if((long) price / ageInMonths < 3){
            ageInMonths --;
        }
        osamaksed.add((long) price / ageInMonths);
        for(long i = 1; i < ageInMonths; i++){
            if(jaak % 2 == 0){
                osamaksed.add((long) price / ageInMonths + jaak/(ageInMonths-1));
            }else{
                if(i == ageInMonths-1){
                    osamaksed.add((long) price / ageInMonths + jaak);
                }
            }
        }

        ArrayList<Installments> inst = new ArrayList<>();
        int i = 0;
        for(Long os : osamaksed){
            if(i > 0){
                inst.add(new Installments(os.intValue(),start.withDayOfMonth(1).plusMonths(i)));

            }else{
                inst.add(new Installments(os.intValue(),start));
            }
            i++;
        }
        return inst;
    }
}


