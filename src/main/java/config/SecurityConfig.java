package config;

import config.handlers.ApiAccessDeniedHandler;
import config.handlers.ApiEntryPoint;
import config.handlers.ApiLogoutSuccessHandler;
import config.jwt.JwtAuthenticationFilter;
import config.jwt.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@EnableWebSecurity
@PropertySource("classpath:/application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${jwt.signing.key}")
    private String jwtKey;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers("/api/version").permitAll()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/**").authenticated()
        ;

        http.exceptionHandling()
                .authenticationEntryPoint(new ApiEntryPoint());

        http.exceptionHandling()
                .accessDeniedHandler(new ApiAccessDeniedHandler());

        //http.formLogin();

        System.out.println(jwtKey);
        var jwtLoginFilter = new JwtAuthenticationFilter(
                authenticationManager(), "/api/login",jwtKey);

        var jwtAuthFilter = new JwtAuthorizationFilter(authenticationManager(), jwtKey);

        http.addFilterBefore(jwtAuthFilter, LogoutFilter.class);
        http.addFilterAfter(jwtLoginFilter, LogoutFilter.class);

        http.logout().logoutUrl("/api/logout");
        http.logout().logoutSuccessHandler(new ApiLogoutSuccessHandler());

    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("user").password("{noop}user").roles("USER")
        .and()
                .withUser("admin").password("{noop}admin").roles("USER","ADMIN");
    }
}
